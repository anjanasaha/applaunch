//
//  RootSetupStep.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import Foundation
import UserInterface

public class RootSetupStep: RootSetupStepType {
       
    public override init() {
    }
    public override func run(input: (window: UIWindow, dependency: RootDependency)) -> RootRouting {
        let routerBuilder = RootBuilder(dependency: input.dependency, window: input.window)
        let rootRouter = routerBuilder.build(withListener: nil)
        rootRouter.launch(from: input.window)
        return rootRouter
    }
}

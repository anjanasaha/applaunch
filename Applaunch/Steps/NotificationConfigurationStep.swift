//
//  NotificationConfigurationStep.swift
//  Hopshop
//
//  Created by Deborshi Saha on 9/8/19.
//  Copyright © 2019 Deborshi Saha. All rights reserved.
//

import Foundation
import UserNotifications

public typealias NotificationConfigurationStepResult = ()
public typealias NotificationConfigurationStepType = ApplicationLaunchStep<(), NotificationConfigurationStepResult>

public final class NotificationConfigurationStep: NotificationConfigurationStepType {
    
    public override init() { }
    
    public override func run(input: ()) -> NotificationConfigurationStepResult {
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
//            (granted, error) in
//            if granted {
//                DispatchQueue.main.async {
//                    Messaging.messaging().delegate = self.notificationManager
//                    self.application.registerForRemoteNotifications()
//                }
//            }
//        }
    }
}

//class NotificationManager: NSObject, NotificationManaging {
//
//    func update(status: NotificationStatus) {
//        //
//    }
//    
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print("Did receive notification")
//    }
//
//    /// MARK - MessagingDelegate
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//
//        print("fcmToken \(fcmToken)")
//
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//            }
//        }
//    }
//    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("remoteMessage \(remoteMessage)")
//    }
//    
//    deinit {
//        print("HAHAHAHA")
//    }
//}
//
//protocol NotificationManaging: class, UNUserNotificationCenterDelegate, MessagingDelegate {
//   // var status: Observable<NotificationStatus> { get set }
//    
//    func update(status: NotificationStatus)
//}
//
//public enum NotificationStatus {
//    case permitted
//    case notAllowed
//    case unknown
//}

//
//  FirebaseConfigurationStep.swift
//

import Foundation

public typealias ThirdPartyConfigurationStepResult = ()

public typealias ThirdpartyConfigurationStepExecutionBlock = (() -> ())

public typealias ThirdPartyConfigurationStepType = ApplicationLaunchStep<ThirdpartyConfigurationStepExecutionBlock?, ThirdPartyConfigurationStepResult>

public final class ThirdPartyConfigurationStep: ThirdPartyConfigurationStepType {
    
    public override init() { }
    
    public override func run(input: ThirdpartyConfigurationStepExecutionBlock?) -> ThirdPartyConfigurationStepResult {
        input?()
    }
}

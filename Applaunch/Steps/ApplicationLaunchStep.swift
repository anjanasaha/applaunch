//
//  ApplicationLaunchStep.swift
//  Hopshop
//
//  Created by Deborshi Saha on 9/8/19.
//  Copyright © 2019 Deborshi Saha. All rights reserved.
//

import Foundation

open class ApplicationLaunchStep<ParameterType, ReturnType> {
    
    open func run(input: ParameterType) -> ReturnType {
        fatalError("Application Launch Step")
    }
}

//
//  WindowStep.swift
//

import UIKit

public typealias WindowStepType = ApplicationLaunchStep<(), UIWindow?>

public final class WindowStep: WindowStepType {
    
    var window: UIWindow?
    
    public override init() { }
    
    public override func run(input: ()) -> UIWindow? {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()

        return window
    }
}

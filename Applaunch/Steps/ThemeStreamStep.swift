//
//  ThemeStreamStep.swift
//  Hopshop
//

import UserInterface

public typealias ThemeStreamStepType = ApplicationLaunchStep<ThemeConfiguration, ThemeStreaming>

public final class ThemeStreamStep: ThemeStreamStepType {
    
    public override init() { }
    
    public override func run(input: ThemeConfiguration) -> ThemeStreaming {
        return ThemeStream(themeType: (UIScreen.main.traitCollection.userInterfaceStyle == .dark) ? .dark : .light  )
    }
}

final public class ThemeConfiguration {
    
    let automaticTheming: Bool
    
    // let resource: Resource
    
    init(automaticTheming: Bool) {
        self.automaticTheming = automaticTheming
    //    self.resource = resource
    }
}



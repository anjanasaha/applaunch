//
//  Applaunch.h
//  Applaunch
//
//  Created by Deborshi Saha on 7/25/20.
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Applaunch.
FOUNDATION_EXPORT double ApplaunchVersionNumber;

//! Project version string for Applaunch.
FOUNDATION_EXPORT const unsigned char ApplaunchVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Applaunch/PublicHeader.h>



//
//  AppLaunchManager.swift
//

import RIBs
import UserNotifications
import RxSwift
import UserInterface
import AuthenticationCommon

public typealias RootDependencyStepType = ApplicationLaunchStep<ThemeStreaming, RootDependency>

public typealias RootSetupStepType = ApplicationLaunchStep<(window: UIWindow, dependency: RootDependency), RootRouting>

open class BaseRootDependencyStep: RootDependencyStepType {
    
    public override init() {}
}

public final class AppLaunchManager: RootListener {
    
    private let PLACES_AUTOCOMPLETE_API_KEY = "AIzaSyCb5MVg3g-d7sBC7E6PSVvwA-Zfzqb-93M" // AIzaSyCb5MVg3g-d7sBC7E6PSVvwA-Zfzqb-93M

//    var application: UIApplication
//    var window: UIWindow?
    private var windowStep : WindowStepType
    private var thirdPartyConfigurationStep: ThirdPartyConfigurationStepType
    private var notificationConfigurationStep: NotificationConfigurationStepType
    private var rootSetupStep: RootSetupStepType
    private var themeStreamStep: ThemeStreamStepType
    private var rootDependencyStep: RootDependencyStepType
    
    var theRootRouter: RootRouting?
    
    public init(_ windowStep: WindowStepType,
         thirdPartyConfigurationStep: ThirdPartyConfigurationStepType,
//         googlePlaceConfigurationStep: GooglePlaceConfigurationStepType,
         notificationConfigurationStep: NotificationConfigurationStepType,
         themeStreamStep: ThemeStreamStepType,
         rootDependencyStep: RootDependencyStepType,
         rootSetupStep: RootSetupStepType) {
        self.windowStep = windowStep
        self.thirdPartyConfigurationStep = thirdPartyConfigurationStep
        self.notificationConfigurationStep = notificationConfigurationStep
        self.themeStreamStep = themeStreamStep
        self.rootDependencyStep = rootDependencyStep
        self.rootSetupStep = rootSetupStep
    }

    public func launch(_ application: UIApplication, _ executionBlock: ThirdpartyConfigurationStepExecutionBlock?) {
        runSteps(application, executionBlock)
    }
    
    final private func runSteps(_ application: UIApplication, _ executionBlock: ThirdpartyConfigurationStepExecutionBlock? = nil) {
        guard let window = windowStep.run(input: ()) else { return }
        _ = thirdPartyConfigurationStep.run(input: executionBlock)
        let themeStream = themeStreamStep.run(input: ThemeConfiguration(automaticTheming: true) )
        let rootDependency = rootDependencyStep.run(input: (themeStream))
        _ = notificationConfigurationStep.run(input: ())
        theRootRouter = rootSetupStep.run(input: (window: window, dependency: rootDependency))
    }
}

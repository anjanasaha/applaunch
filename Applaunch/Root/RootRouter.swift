//
//  RootRouter.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import RIBs
import AuthenticationCommon
import AuthenticationUI
import UserInterface

public protocol RootRouting: LaunchRouting {
    func routeToLoggedin()
    func routeAwayFromLoggedin()

    func routeToLoggedout()
    func routeAwayFromLogout()
    
    func routeToRegistrationFlow(user: UserEntity)
    func routeAwayFromRegistrationFlow()
}

protocol RootInteractable: Interactable, SignedOutListener, SignedInListener, RegistrationFlowListener {
    var router: RootRouting? { get set }
    var listener: RootListener? { get set }
}

protocol RootViewControllable: ViewControllable { }

final class RootRouter: LaunchRouter<RootInteractable, RootViewControllable>, RootRouting {

    private weak var window: UIWindow?
    private var navigationController: NavigationController?

    let themeStream : ThemeStreaming

    let signInBuilder: SignedInBuildable
    let signedOutBuilder: SignedOutBuildable
    let registrationFlowBuilder: RegistrationFlowBuildable
    let registrationFlowViewController: RegistrationFlowViewControllable
    
    weak var registrationFlowRouter: RegistrationFlowRouting?
    weak var loggedInRouter: SignedInRouting?
    weak var loggedOutRouter: SignedOutRouting?
    
    func routeToLoggedin() {
        print("\(#function)")
    }
    
    func routeAwayFromLogout() {
        print("\(#function)")
    }
    
    func routeAwayFromLoggedin() {
        print("\(#function)")
        guard let router = loggedInRouter else { return }
        print("\(#function) 1")
        detachChild(router)
        let loginViewController = router.viewControllable.uiviewController
        loginViewController.dismiss(animated: true, completion: nil)
        loggedInRouter = nil
    }

    func routeToLoggedout() {
        print("\(#function)")
        guard loggedOutRouter == nil else { return }
        print("\(#function) 2")
        let router = signedOutBuilder.build(withListener: interactor)
        attachChild(router)
        loggedOutRouter = router
        let logoutViewController = router.viewControllable.uiviewController
        let navController = NavigationController(rootViewController: logoutViewController, themeStream: themeStream)
        viewControllable.uiviewController.presentInFullScreen(navController, animated: false, completion: nil)
        navigationController = navController
    }

    func routeAwayFromRegistrationFlow() {
        print("\(#function)")
        guard let theRouter = registrationFlowRouter else { return }
        print("\(#function) 1")
        detachChild(theRouter)
        registrationFlowRouter = nil
    }
    
    func routeToRegistrationFlow(user: UserEntity ) {
        print("\(#function)")
        guard registrationFlowRouter == nil else { return }
        let router = registrationFlowBuilder.build(withListener: interactor, user: user, registrationFlowViewController: registrationFlowViewController)
        attachChild(router)
        registrationFlowRouter = router
    }

    public init(window: UIWindow?,
                interactor: RootInteractable,
                viewController: RootViewControllable,
                themeStream : ThemeStreaming,
                signInBuilder: SignedInBuildable,
                signedOutBuilder: SignedOutBuildable,
                registrationFlowBuilder: RegistrationFlowBuildable,
                registrationFlowViewController: RegistrationFlowViewControllable) {
        self.window = window
        self.registrationFlowViewController = registrationFlowViewController
        self.themeStream = themeStream
        self.signInBuilder = signInBuilder
        self.signedOutBuilder = signedOutBuilder
        self.registrationFlowBuilder = registrationFlowBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}

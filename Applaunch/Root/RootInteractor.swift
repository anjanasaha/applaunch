//
//  RootInteractor.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import RIBs
import RxSwift
import UserInterface
import AuthenticationCommon
import AuthenticationUI

public protocol RootDependency: SignedInDependency, SignedOutDependency, RegistrationFlowDependency {
    var signedInBuilder: SignedInBuildable? { get }
    var signedOutBuilder: SignedOutBuildable? { get }
    var registrationFlowBuilder: RegistrationFlowBuildable? { get }
}

public protocol RootPresentable: Presentable {
    var listener: RootPresentableListener? { get set }
    // TODO: Declare methods the interactor can invoke the presenter to present data.
}

public protocol RootListener: class { }

public final class RootInteractor: PresentableInteractor<RootPresentable>, RootInteractable, RootPresentableListener {

    weak var router: RootRouting?
    weak var listener: RootListener?

    private let authenticationService: AuthenticationServicing

    init(presenter: RootPresentable, backendService: AuthenticationServicing) {
        self.authenticationService = backendService
        super.init(presenter: presenter)
        presenter.listener = self
    }

    public override func didBecomeActive() {
        super.didBecomeActive()

        authenticationService.authenticationStatus.subscribe(onNext: { [weak self] (status) in
            switch status {
            case .loggedIn(let result):
                switch result {
                case .success(let user):
                    if !user.isNew {
                        self?.router?.routeAwayFromLogout()
                        self?.router?.routeToLoggedin()
                    } else {
                        self?.router?.routeAwayFromLogout()
                        self?.router?.routeToRegistrationFlow(user: user)
                    }
                case .failure(_):
                    self?.router?.routeAwayFromRegistrationFlow()
                    self?.router?.routeAwayFromLoggedin()
                    self?.router?.routeToLoggedin()
                }
            case .loggedOut:
                self?.router?.routeAwayFromRegistrationFlow()
                self?.router?.routeAwayFromLoggedin()
                self?.router?.routeToLoggedout()
                break
            }
        }, onError: { [weak self] error in
            self?.router?.routeToLoggedout()
        }).disposeOnDeactivate(interactor: self)

        authenticationService.checkLogin()
    }

    public func loginSuccess() {
        router?.routeToLoggedin()
    }
    
    public func registrationFlowComplete() {
        router?.routeAwayFromRegistrationFlow()
        router?.routeToLoggedin()
    }

    func addingEmailPasswordDidSucceed() {
        // NO-OP
        // router?.routeAwayFromRegistrationFlow()
    }
    
}


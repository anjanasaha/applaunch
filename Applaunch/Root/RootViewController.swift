//
//  RootViewController.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import RIBs
import RxSwift
import UserInterface
import AuthenticationUI

public protocol RootPresentableListener: class { }

final class RootViewController: ViewController, RootPresentable, RootViewControllable, RegistrationFlowViewControllable {

    weak var listener: RootPresentableListener?
    var vc: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func presentVC(viewController: UIViewController) {
        vc = viewController
        presentInFullScreen(viewController, animated: false, completion: nil)
    }
    
    func dismissVC() {
        vc?.dismiss(animated: true, completion: nil)
    }
}

//
//  RootBuilder.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import RIBs
import UserInterface
import AuthenticationCommon
import AuthenticationUI

public final class RootComponent: Component<RootDependency> {

    var themeStream: ThemeStreaming {
        return dependency.themeStream
    }
    
    var authenticationService: AuthenticationServicing {
        return dependency.authenticationService
    }
    
    var defaultSignInBuilder: SignedInBuildable {
        return DefaultSignedInBuilder(dependency: dependency)
    }
    
    var defaultSignedOutBuilder: SignedOutBuildable {
        return DefaultSignedOutBuilder(dependency: dependency)
    }
    
    var defaulrRegistrationFlowBuilder: RegistrationFlowBuildable {
        return DefaultRegistrationFlowBuilder(dependency: dependency)
    }
}

// MARK: - Builder

public protocol RootBuildable: Buildable {
    func build(withListener listener: RootListener?) -> RootRouting
}

public final class RootBuilder: Builder<RootDependency>, RootBuildable {

    private weak var window: UIWindow?

    public init(dependency: RootDependency, window: UIWindow?) {
        self.window = window
        super.init(dependency: dependency)
    }

    public func build(withListener listener: RootListener?) -> RootRouting {
        let component = RootComponent(dependency: dependency)
        let viewController = RootViewController(themeStream: component.themeStream)
        let interactor = RootInteractor(presenter: viewController, backendService: component.authenticationService)
        interactor.listener = listener

        let signInBuilder: SignedInBuildable = component.dependency.signedInBuilder ?? component.defaultSignInBuilder
        
        let signedOutBuilder: SignedOutBuildable = component.dependency.signedOutBuilder ?? component.defaultSignedOutBuilder
        
        let registrationFlowBuilder: RegistrationFlowBuildable = component.dependency.registrationFlowBuilder ?? component.defaulrRegistrationFlowBuilder
        
        return RootRouter(window: self.window,
                          interactor: interactor,
                          viewController: viewController,
                          themeStream: component.themeStream,
                          signInBuilder: signInBuilder,
                          signedOutBuilder: signedOutBuilder,
                          registrationFlowBuilder:registrationFlowBuilder,
                          registrationFlowViewController: viewController)

    }
}

